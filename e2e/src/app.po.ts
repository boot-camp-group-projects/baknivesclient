import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText() as Promise<string>;
  }

  getFirstInput(){
    return element(by.xpath('/html[1]/body[1]/div[1]/div[1]/form[1]/input[1]'));
  }

  getSecondInput(){
    return element(by.xpath('/html[1]/body[1]/div[1]/div[1]/form[1]/select[1]'));
  }

  getThirdInput(){
    return element(by.xpath('/html[1]/body[1]/div[1]/div[1]/form[1]/input[2]'));
  }
  clickGo(){
    return element(by.xpath('/html[1]/body[1]/div[1]/div[1]/form[1]/button[1]'));
  }
  getFirstResult(){
    return element(by.xpath('/html[1]/body[1]/div[1]/table[1]/tbody[1]/tr[1]/td[3]'));
  }

}
