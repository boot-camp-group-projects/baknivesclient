describe('category_bar_test', function () {

  it('should_load_page', function () {
    cy.visit('http://localhost:4200');
  });

  it('should_contain_combat_knives_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Combat Knives').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_custom_knives_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Custom Knives').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_automatic_knives_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Automatic Knives').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_fixed_blade_knives_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Fixed Blade Knives').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_hunting_knives_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Hunting Knives').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_pens_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Pens').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_swords_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Swords').click();
    cy.url().should('include', '/products')
  });

  it('should_contain_home_button', function () {
    cy.visit('http://localhost:4200');
    cy.contains('Home').click();
    cy.url().should('include', '/home')
  });

});
