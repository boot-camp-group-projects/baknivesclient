describe('my_account_tests',function () {
  it(
    'should load page', function () {
      cy.visit('http://localhost:4200')
    }
  )

  it(
    'Should allow for internal navigation', function () {
      cy.visit('http://localhost:4200')
      cy.contains('My Account').click()
      // cy.url().should('include','myaccount')
    }
  )
});

describe('my account information',function () {
  it(
    'should load page', function () {
      cy.visit('http://localhost:4200/myAccount')
      cy.contains('Update information').click()
      cy.get('#Name').type('Crocodile Dundee')
      cy.get('#Address').type('ausie road')
      cy.get('#Email').type('ausie@gmail.com')
      cy.get('#Phone').type('911')
      cy.get('#Password').type('1234')
      cy.contains('Update information').click()
    }
  )

});

