
describe('manufacturer_image_test', function () {

  it('should_contain_benchmade_logo', function () {
    cy.visit('http://localhost:4200');
    cy.get('.imgContainer').find('img').eq(0).click();
    cy.url().should('include', '/products')
  });

  it('should_contain_microtech_logo', function () {
    cy.visit('http://localhost:4200');
    cy.get('.imgContainer').find('img').eq(1).click();
    cy.url().should('include', '/products')
  });

  it('should_contain_weknife_logo', function () {
    cy.visit('http://localhost:4200');
    cy.get('.imgContainer').find('img').eq(2).click();
    cy.url().should('include', '/products')
  });

  it('should_contain_spyderco_logo', function () {
    cy.visit('http://localhost:4200');
    cy.get('.imgContainer').find('img').eq(3).click();
    cy.url().should('include', '/products')
  });

  it('should_contain_chrisreeveknife_logo', function () {
    cy.visit('http://localhost:4200');
    cy.get('.imgContainer').find('img').eq(4).click();
    cy.url().should('include', '/products')
  });

});

