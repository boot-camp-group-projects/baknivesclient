import { Component, OnInit } from '@angular/core';
import {MyaccountService} from '../services/myaccount.service';
import {FormControl, FormGroup} from '@angular/forms';
@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {
  newCustomer: FormGroup;
  constructor(private service: MyaccountService) { }


  ngOnInit() {
    this.newCustomer = new FormGroup({
      name: new FormControl(),
      address: new FormControl(),
      zipcode: new FormControl(),
      email: new FormControl(),
      phone: new FormControl(),
      password: new FormControl()
    });
  }
  public createCustomer() {
    const Ob = {
      name: this.newCustomer.controls.name.value,
      address: this.newCustomer.controls.address.value,
      zipcode: this.newCustomer.controls.zipcode.value,
      email: this.newCustomer.controls.email.value,
      phone: this.newCustomer.controls.phone.value,
      password: this.newCustomer.controls.password.value
    };
    this.service.createCustomer(Ob).subscribe();
    this.newCustomer.reset();
  }
}
