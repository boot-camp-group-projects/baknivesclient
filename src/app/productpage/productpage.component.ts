import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../services/category.service';

@Component({
  selector: 'app-productpage',
  templateUrl: './productpage.component.html',
  styleUrls: ['./productpage.component.css']
})
export class ProductpageComponent implements OnInit {
  products: Array<any>;

  constructor(private category: CategoryService) {
    this.category.productData$.subscribe(data => {this.products = data; });
  }

  ngOnInit() {
  }

}
