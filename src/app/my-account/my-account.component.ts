import { Component, OnInit } from '@angular/core';
import {MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
customers: Array<any>;
  constructor(private customer: MyaccountService) { }

  ngOnInit() {
    this.customer.getAllCustomers().subscribe(data => {this.customers = data; });
  }

}


