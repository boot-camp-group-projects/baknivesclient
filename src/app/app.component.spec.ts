import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {CategorybarComponent} from "./categorybar/categorybar.component";
import {CreateAccountComponent} from "./create-account/create-account.component";
import {HomeComponent} from "./home/home.component";
import {CustomerHistoryComponent} from "./customer-history/customer-history..component";
import {NavbarComponent} from "./navbar/navbar.component";
import {MyAccountComponent} from "./my-account/my-account.component";
import {SearchbarComponent} from "./searchbar/searchbar.component";
import {FooterComponent} from "./footer/footer.component";
import {ProductpageComponent} from "./productpage/productpage.component";
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatGridListModule} from "@angular/material";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NavbarComponent,
        HomeComponent,
        MyAccountComponent,
        SearchbarComponent,
        FooterComponent,
        CategorybarComponent,
        ProductpageComponent,
        CustomerHistoryComponent,
        CreateAccountComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatGridListModule,
        RouterTestingModule ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'BAKnivesClient'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('BAKnivesClient');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to BAKnivesClient!');
   });
});
