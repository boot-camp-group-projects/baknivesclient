import { Component, OnInit } from '@angular/core';
import {MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-customer-history',
  templateUrl: './customer-history.component.html',
  styleUrls: ['./customer-history.component.css']
})
export class CustomerHistoryComponent implements OnInit {
orders: Array<any>;
customer;
  constructor(private order: MyaccountService) { }

  ngOnInit() {
    this.order.getOrderByCustomerId(this.customer.id).subscribe(data => {this.orders = data; });
  }

}

