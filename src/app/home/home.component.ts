import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../services/category.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  manufacturers: Array<any>;
  random5Products: Array<any>;

  constructor(private category: CategoryService, private router: Router) { }

  ngOnInit() {
    this.category.getAllManufacturers().subscribe(manu => {this.manufacturers = manu; });
    this.category.get5RandomProducts().subscribe(data => { this.random5Products = data; });
  }
  clickManufacturer(manufacturerId: number) {
    this.category.setManufacturerId(manufacturerId);
    this.router.navigate(['/', 'products']);
  }
  clickProduct(productId: number) {
  }
}
