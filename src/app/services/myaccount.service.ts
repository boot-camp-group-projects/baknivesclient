import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyaccountService {

  constructor(private http: HttpClient) { }

  public getAllCustomers(): Observable<any> {
    return this.http.get<any>('//localhost:8080/customers');
  }
  public getCustomerById(id: number): Observable<any> {
    return this.http.get<any>('//localhost:8080/customers' + id);
  }
  public createCustomer(customer: object) {
    return this.http.post('//localhost:8080/customers/create', customer);
  }
  public getOrderByCustomerId(id: number): Observable<any>{
    return this.http.get<any>('//localhost:8080/orders' + id);
  }
}

