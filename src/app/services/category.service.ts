import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private productData = new BehaviorSubject<Array<any>>(null);
  productData$ = this.productData.asObservable();
  private manufacturerId = new BehaviorSubject<number>(1);
  manufacturerId$ = this.manufacturerId.asObservable();
  private categoryId = new BehaviorSubject<number>(1);
  categoryId$ = this.categoryId.asObservable();


  constructor(private http: HttpClient) {

  }

  getAllCategories(): Observable<any> {
  return this.http.get<any>('//localhost:8080/categories'); }

  getProductsByCategoryId(id: number): Observable<any> {
    return this.http.get<any>('//localhost:8080/products/categories/' + id);
  }

  setProductData(data) {
    this.productData.next(data);
  }
  setCategoryId(categoryId) {
    this.categoryId.next(categoryId);
    this.getProductsByCategoryId(categoryId).subscribe(data => {this.setProductData(data);
    });
  }
  getAllManufacturers(): Observable<any> {
    return this.http.get('//localhost:8080/manufacturers'); }

  getManufacturerById(id: number): Observable<any> {
  return this.http.get('//localhost:8080/manufacturers/' + id ); }

  getProductsByManufacturerId(manufacturerId: number): Observable<any> {
    return this.http.get('//localhost:8080/products/manufacturers/' + manufacturerId);
  }

  get5RandomProducts(): Observable<any> {
    return this.http.get('//localhost:8080//products/random5');
  }

  setManufacturerProductData(data) {
    this.productData.next(data);
  }

  setManufacturerId(manufacturerId) {
    this.manufacturerId.next(manufacturerId);
    this.getProductsByManufacturerId(manufacturerId).subscribe(data => this.setProductData(data));
  }

}
