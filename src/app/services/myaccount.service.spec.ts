import { TestBed } from '@angular/core/testing';

import { MyaccountService } from './myaccount.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('MyaccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule ],
    declarations: []
  }));

  it('should be created', () => {
    const service: MyaccountService = TestBed.get(MyaccountService);
    expect(service).toBeTruthy();
  });
});
