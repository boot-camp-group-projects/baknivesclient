import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  username: string;
  // private password = new BehaviorSubject(<string>)()
  // password$ = this.password.asObservable();

  constructor(private router: Router) { }



  ngOnInit() {
// Get the modal
    const modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
    window.onclick = (event) => {
        if (event.target === modal) {
          modal.style.display = 'none';
        }
      };
  }
  // login(): void {
  //   if(this.username == 'admin' && this.password == 'admin'){
  //     this.router.navigate(["myAccount"]);
  //   }else{
  //     alert("Invalid credentials");
  //   }
  //
  //     }
}


