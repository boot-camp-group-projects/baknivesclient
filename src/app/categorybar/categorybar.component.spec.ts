import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorybarComponent } from './categorybar.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";

describe('CategorybarComponent', () => {
  let component: CategorybarComponent;
  let fixture: ComponentFixture<CategorybarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [ HttpClientTestingModule,
                  RouterTestingModule],
      declarations: [ CategorybarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorybarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
