import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../services/category.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-catergorybar',
  templateUrl: './categorybar.component.html',
  styleUrls: ['./categorybar.component.css']
})
export class CategorybarComponent implements OnInit {
  categories: Array<any>;
  products: Array<any>;

  constructor(private categoryService: CategoryService, private router: Router) {
  }

  ngOnInit() {
    this.categoryService.getAllCategories().subscribe(data => {
      this.categories = data;
    });
  }

  clickCategory(categoryId: number) {
    this.categoryService.setCategoryId(categoryId);
    this.router.navigate(['/', 'products']);
  }



}




