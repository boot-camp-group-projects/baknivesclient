import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {MyAccountComponent} from './my-account/my-account.component';
import {ProductpageComponent} from './productpage/productpage.component';
import {CreateAccountComponent} from './create-account/create-account.component';
import {CustomerHistoryComponent} from './customer-history/customer-history.component';
import {ItempageComponent} from './itempage/itempage.component';
import {NavbarComponent} from "./navbar/navbar.component";

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
   {path: 'myAccount', component: MyAccountComponent},
  {path: 'products', component: ProductpageComponent},
  {path: 'createAccount', component: CreateAccountComponent},
  {path: 'customerHistory',component: CustomerHistoryComponent},
  {path: 'item', component: ItempageComponent},
  {path: 'navbar',component: NavbarComponent},
  {path: '',component: NavbarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
